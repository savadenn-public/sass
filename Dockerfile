FROM node:12

RUN npm install -g sass

ENTRYPOINT ["sass"]
CMD ["."]
